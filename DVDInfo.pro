#-------------------------------------------------
#
# Project created by QtCreator 2015-01-26T22:23:47
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = DVDInfo
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    filemanip.cpp \
    movie.cpp

HEADERS += \
    filemanip.h \
    movie.h

#RESOURCES += \
#    database.qrc
